# u-werkwoorden (godan)

す -> し
つ -> ち

| werkwoord |  formeel   | vertaling  |
|:---------:|:----------:|:----------:|
|  さそう   | さそいます | uitnodigen |

# ru-werkwoorden (ichidan)

| werkwoord |  formeel   |      vertaling      |
|:---------:|:----------:|:-------------------:|
| つとめる  | つとめます | tewerk gesteld zijn |

# Onregelmatige werkwoorden

| werkwoord |  formeel   | vertaling  |
|:---------:|:----------:|:----------:|
