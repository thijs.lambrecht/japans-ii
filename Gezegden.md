
# Gezegden

|              japans              |                             vertaling                              |
|:--------------------------------:|:------------------------------------------------------------------:|
|           はじめまして           |                wanneer je u begint voor te stellen                 |
| どうぞ　よろしく　おねがいします |                waneer je eindigt je voor te stellen                |
|           いってきます           |                  wanneer ik vertrek:  いく + くる                  |
|          いってらしゃい          |       wanneer iemand anders vertrekt, antwoord op hierboven        |
|             ただいま             |                            ik ben terug                            |
|          おかえりなさい          | welkom terug, als iemand thuis komt antwoord op hierboven:  かえる |
|          しつれいします          |             wanneer je iemand anders deur binnen gaat              |
|           おじゃました           |         wanneer je uw schoenen uit doet bij iemand anders          |
|             ようこそ             |                               welkom                               |
|         いらっしゃいませ         |                     welkom, bvb in een winkel                      |
