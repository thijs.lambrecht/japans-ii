
# Vocabulair

## Eten

|  japans  |   vertaling    |
|:--------:|:--------------:|
|   のり   | nori (zeewier) |
|   まめ   |     bonen      |
|  くじら  |     walvis     |
|  すいか  |  watermeloen   |
| にんじん |     wortel     |

## Voorwerpen

|  japans  |   vertaling   |
|:--------:|:-------------:|
|  めがね  |     bril      |
|   かさ   |    paraplu    |
|  リボン  |     strik     |
|  めいし  | visitekaartje |
| こくばん | (school)bord  |
| ほんだな |  boekenplank  |

## Dieren

| japans | vertaling |
|:------:|:---------:|
|  あり  |   mier    |
|  りす  | eekhoorn  |
| くじら |  walvis   |
|  さる  |    aap    |
| もぐら |    mol    |

## Varia

|   japans   |     vertaling      |
|:----------:|:------------------:|
|   どちら   |        welk        |
|  おいくつ  |  hoe oud ben je?   |
|   おじぎ   |   lichte buiging   |
| ふくしゅう | herhalingsoefening |
|  さくぶん  |       opstel       |
|   あいだ   |      terwijl       |
| きょうだい |      siblings      |
|  さいごに  |     tenslotte      |
|    まち    |        stad        |
